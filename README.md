## Customer-App

**Installation**

 1. Install Python 3 pip:

	    sudo apt-get install python3-pip

 2. Install virtualenv:

		sudo pip3 install virtualenv

 3. Clone git repo:

		git clone https://gitlab.com/aderuyter/customer-app.git customer-app

 4. Start virtualenv for project:

		cd customer-app && python3 -m venv myvenv && source myvenv/bin/activate

 5. Install requirements:

		pip3 install -r requirements.txt

 6. Run server

		python3 manage.py runserver

 7. Run makemigrations and migrate
 
		 python3 manage.py makemigrations
		 pyhton3 manage.py migrate

8. Create superuser

		python3 manage.py createsuperuser
