from django.db import models
# For UniqueConstraint (https://docs.djangoproject.com/en/3.1/ref/models/constraints/#uniqueconstraint)
from django.db.models import Q
# For Google Maps API (https://github.com/googlemaps/google-maps-services-python)
import googlemaps
# For decimal numbers (https://docs.python.org/3/library/decimal.html)
from decimal import Decimal
# For checking validity of VAT numbers (https://django-internationalflavor.readthedocs.io/en/latest/vat_number.html)
from internationalflavor.vat_number import VATNumberField
# For creating a standardized select-list of countries for models (https://pypi.org/project/django-countries/)
from django_countries.fields import CountryField

# Initialize Google Maps Client API
gmaps = googlemaps.Client(key='AIzaSyB-qTCe9yan-jtmuB2NvlrhhLIhp8wz7N4')


# Create Address model
class Address(models.Model):
    street = models.CharField(max_length=100)
    number = models.IntegerField(null=True)
    postal = models.IntegerField(null=True)
    city = models.CharField(max_length=100)
    country = CountryField()
    is_primary = models.BooleanField(null=False)
    geo_lat = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    geo_lon = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    # Add a many-to-one relation with ForeignKey (https://docs.djangoproject.com/en/3.1/topics/db/examples/many_to_one/)
    customer = models.ForeignKey("Customer", on_delete=models.CASCADE, related_name="addresses")

    
    class Meta:
        # Add verbose name plural to get rid of the extra 's' behind the name in Admin
        verbose_name_plural = 'Addresses'
        # Create UniqueConstraint 'unique_primary_per_customer' for 'is_primary'
        constraints = [
            models.UniqueConstraint(
                fields=['customer'],
                condition=Q(is_primary=True),
                name='unique_primary_per_customer'
            )
        ]


    # Use a custom save procedure to check and alter data
    def save(self, *args, **kwargs):
        if self.street and self.number and self.postal and self.city and self.country:
            str_address = f"{self.street} {self.number}, {self.postal} {self.city}, {self.country}"
            # Look up address through Google Maps Geocode API
            geocode_result = gmaps.geocode(str_address)
            # Covert to decimal rounded to 6 decimals
            geocode_lat = round(Decimal(geocode_result[0]["geometry"]["location"]["lat"]), 6)
            geocode_lon = round(Decimal(geocode_result[0]["geometry"]["location"]["lng"]), 6)

            # Assign values to the correct fields if the values don't match with the lookup
            if self.geo_lat != geocode_lat and self.geo_lon != geocode_lon:
                self.geo_lat = geocode_lat
                self.geo_lon = geocode_lon

        # Check if address is primary
        if self.is_primary:
            self.__class__._default_manager.filter(customer=self.customer, is_primary=True).update(is_primary=False)

        # Save the changes
        super().save(*args, **kwargs)


    # Display nice values in table
    def __str__(self):
        return f"{self.street} {self.number}, {self.postal} {self.city}, {self.country}"


# Create Customer model
class Customer(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    vat = VATNumberField(countries=['NL', 'BE', 'FR', 'DE', 'UK'], blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    # Display nice values in table
    def __str__(self):
        return f"{self.name}"


# Create Customerview model
class Customerview(models.Model):
    name = models.CharField(max_length=100, db_column='name')
    email = models.EmailField(unique=True, db_column='email')
    vat = VATNumberField(countries=['NL', 'BE', 'FR', 'DE', 'UK'], blank=True, null=True, db_column='vat')
    street = models.CharField(max_length=100, db_column='street')
    number = models.IntegerField(null=True, db_column='number')
    postal = models.IntegerField(null=True, db_column='postal')
    city = models.CharField(max_length=100, db_column='city')
    country = CountryField(db_column='country')
    is_primary = models.BooleanField(null=False, db_column='is_primary')
    geo_lat = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True, db_column='geo_lat')
    geo_lon = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True, db_column='geo_lon')


    class Meta:
        verbose_name_plural = 'View Customers'
        managed = False
        db_table = 'customerview'
