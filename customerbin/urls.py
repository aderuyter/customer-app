from django.urls import path
from . import views


urlpatterns = [
    path('', views.map, name='map'),
    path('mapjson', views.mapjson, name='mapjson'),
]