# Generated by Django 3.1.7 on 2021-03-27 09:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('customerbin', '0005_auto_20210327_1001'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='customer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='customerbin.customer'),
            preserve_default=False,
        ),
    ]
