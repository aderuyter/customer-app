# Generated by Django 3.1.7 on 2021-03-27 09:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customerbin', '0009_auto_20210327_1042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='geo_lat',
            field=models.DecimalField(blank=True, decimal_places=16, max_digits=22, null=True),
        ),
        migrations.AlterField(
            model_name='address',
            name='geo_lon',
            field=models.DecimalField(blank=True, decimal_places=16, max_digits=22, null=True),
        ),
    ]
