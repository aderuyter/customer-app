# Generated by Django 3.1.7 on 2021-03-27 14:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customerbin', '0011_auto_20210327_1143'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='address',
            options={'verbose_name_plural': 'Addresses'},
        ),
    ]
