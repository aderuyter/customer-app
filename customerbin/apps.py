from django.apps import AppConfig


class CustomerbinConfig(AppConfig):
    name = 'customerbin'
    verbose_name = 'Add, edit or delete Customers'
