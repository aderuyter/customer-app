from django.shortcuts import render
from .models import Customerview
from django.core import serializers
from django.http.response import JsonResponse


def map(request):
    return render(request, 'customerbin/map.html', {})


def mapjson(request):
    queryset_new = Customerview.objects.all()
    serialized_queryset = serializers.serialize('python', queryset_new)
    return JsonResponse(serialized_queryset, safe=False)