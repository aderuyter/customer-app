from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from . import models


@admin.register(models.Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'vat')
    ordering = ('name',)
    search_fields = ('name', 'email', 'vat')


@admin.register(models.Address)
class AddressAdmin(ImportExportModelAdmin):
    list_display = ('customer_name', 'customer_email', 'customer_vat', 'street', 'number', 'postal', 'city', 'country', 'is_primary', 'geo_lat', 'geo_lon')
    list_filter = ('is_primary', 'city')
    ordering = ('street', 'postal', 'city', 'country',)
    search_fields = ('customer__name', 'customer__email', 'customer__vat', 'street', 'postal', 'city')

    def customer_name(self, obj):
        return obj.customer.name
        customer_name.short_description = 'Name'
        customer_name.admin_order_field = 'Customer Name'

    def customer_email(self, obj):
        return obj.customer.email
        customer_email.short_description = 'Email'
        customer_email.admin_order_field = 'Customer Email'

    def customer_vat(self, obj):
        return obj.customer.vat
        customer_vat.short_description = 'VAT'
        customer_vat.admin_order_field = 'Customer VAT'

@admin.register(models.Customerview)
class CustomerviewAdmin(ImportExportModelAdmin):
    list_display = ('name', 'email', 'vat', 'street', 'number', 'postal', 'city', 'country', 'is_primary', 'geo_lat', 'geo_lon')
    list_filter = ('city',)
    readonly_fields = ('name', 'email', 'vat', 'street', 'number', 'postal', 'city', 'country', 'is_primary', 'geo_lat', 'geo_lon',)

    # This will help to disable add functionality
    def has_add_permission(self, request):
        return False

    # This will help to disable delete functionality
    def has_delete_permission(self, request, obj=None):
        return False

    # This will help to disable edit functionality
    def has_change_permission(self, request, obj=None):
        return False
